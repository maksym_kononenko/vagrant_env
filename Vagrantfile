Vagrant.configure("2") do |config|
  config.vm.define "ubuntu-00" do |ubuntu|
    ubuntu.vm.box = "bento/ubuntu-18.04"
    ubuntu.vm.hostname = 'ubuntu-m1'
    ubuntu.vm.box_url = "bento/ubuntu-18.04"
    ubuntu.vm.network "private_network", ip: "192.168.50.20"
    config.vm.provision :shell, :path => "env/setenv.sh"
    ubuntu.vm.network :forwarded_port, guest: 22, host: 10422, id: "ssh"
    config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/", rsync__auto: true
    ubuntu.vm.provider :virtualbox do |v|
      v.cpus = 4
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 3000]
      v.customize ["modifyvm", :id, "--name", "ubuntu-00"]
    end
  end
 
  config.vm.define "ubuntu-01" do |ubuntu|
    ubuntu.vm.box = "bento/ubuntu-18.04"
    ubuntu.vm.hostname = 'ubuntu-s1'
    ubuntu.vm.box_url = "bento/ubuntu-18.04"
    ubuntu.vm.network "private_network", ip: "192.168.50.21"
    config.vm.provision :shell, :path => "env/setenv.sh"
    ubuntu.vm.network :forwarded_port, guest: 22, host: 10522, id: "ssh"
    config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/", rsync__auto: true
    ubuntu.vm.provider :virtualbox do |v|
      v.cpus = 2
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 3000]
      v.customize ["modifyvm", :id, "--name", "ubuntu-01"]
    end
  end
 
  config.vm.define "ubuntu-02" do |ubuntu|
    ubuntu.vm.box = "bento/ubuntu-18.04"
    ubuntu.vm.hostname = 'ubuntu-s2'
    ubuntu.vm.box_url = "bento/ubuntu-18.04"
    ubuntu.vm.network "private_network", ip: "192.168.50.22"
    config.vm.provision :shell, :path => "env/setenv.sh"
    ubuntu.vm.network :forwarded_port, guest: 22, host: 10622, id: "ssh"
    config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/", rsync__auto: true
    ubuntu.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 5000]
      v.customize ["modifyvm", :id, "--name", "ubuntu-02"]
    end
  end
 
  config.vm.define "ubuntu-03" do |ubuntu|
    ubuntu.vm.box = "bento/ubuntu-18.04"
    ubuntu.vm.hostname = 'ubuntu-03'
    ubuntu.vm.box_url = "bento/ubuntu-18.04"
    ubuntu.vm.network "private_network", ip: "192.168.50.23"
    config.vm.provision :shell, :path => "env/setenv.sh"
    ubuntu.vm.network :forwarded_port, guest: 22, host: 10722, id: "ssh"
    config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/", rsync__auto: true
    ubuntu.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 4048]
      v.customize ["modifyvm", :id, "--name", "ubuntu-03"]
    end
  end

  config.vm.define "ubuntu-04" do |ubuntu|
    ubuntu.vm.box = "bento/ubuntu-18.04"
    ubuntu.vm.hostname = 'ubuntu-04'
    ubuntu.vm.box_url = "bento/ubuntu-18.04"
    ubuntu.vm.network "private_network", ip: "192.168.50.24"
    config.vm.provision :shell, :path => "env/setenv.sh"
    ubuntu.vm.network :forwarded_port, guest: 22, host: 10822, id: "ssh"
    config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/", rsync__auto: true
    ubuntu.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 2048]
      v.customize ["modifyvm", :id, "--name", "ubuntu-04"]
    end
  end

  config.vm.define "ubuntu-05" do |ubuntu|
    ubuntu.vm.box = "bento/ubuntu-18.04"
    ubuntu.vm.hostname = 'ubuntu-05'
    ubuntu.vm.box_url = "bento/ubuntu-18.04"
    ubuntu.vm.network "private_network", ip: "192.168.50.25"
    config.vm.provision :shell, :path => "env/setenv.sh"
    ubuntu.vm.network :forwarded_port, guest: 22, host: 10922, id: "ssh"
    config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/", rsync__auto: true
    ubuntu.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 2048]
      v.customize ["modifyvm", :id, "--name", "ubuntu-05"]
    end
  end

  config.vm.define "win-01" do |config|
    config.vm.box = "windows-2016-amd64"
    config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", auto_correct: true
    config.vm.network "private_network", ip: "192.168.50.26"
    config.vm.hostname = "win-01"
    config.vm.communicator = "winrm"
    config.winrm.username = "vagrant"
    config.winrm.password = "vagrant"
    config.vm.guest = :windows
    config.windows.halt_timeout = 15
    config.vm.provision "shell", path: "scripts/install-container-feature.ps1", privileged: true
    config.vm.provision "shell", path: "scripts/enable-autologon.ps1", privileged: true
    config.vm.provision "reload"
    config.vm.provision "shell", path: "scripts/add-docker-group.ps1", privileged: true
    config.vm.provision "shell", path: "scripts/install-docker.ps1", privileged: true
    config.vm.provision "shell", path: "scripts/install-chocolatey.ps1", privileged: false
    config.vm.provision "shell", path: "scripts/install-dockertools.ps1", privileged: false
    config.vm.provision "reload"
    config.vm.provider :virtualbox do |v, override|
      v.customize ["setextradata", "global", "GUI/SuppressMessages", "all" ]
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--name", "win-01"]
      v.gui = true
      v.cpus = 2
      v.memory = 2048
      v.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
      v.customize ["modifyvm", :id, "--draganddrop", "bidirectional"]
    end
  end
end
