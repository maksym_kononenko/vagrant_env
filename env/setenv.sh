#!/usr/bin/env bash

set -e

export DEBIAN_FRONTEND=noninteractive

fatal()
{
    echo "fatal: $@" >&2
}

check_for_root()
{
    if [[ $EUID != 0 ]]; then
        fatal "need to be root"
        exit 1
    fi
}

check_requirements()
{
    if [[ ! -e env/hosts ]]; then
        fatal "hosts not found at /vagrant"
        exit 1
    fi
    if [[ ! -e env/ssh_keys/id_rsa.pub ]]; then
        fatal "Missing ssh keys for setup in env/ssh_keys/"
        exit 1
    fi
}

# All commands expect root access.
check_for_root
check_requirements
# fix the hosts
mkdir -p /root/.ssh
yes | cp -f /vagrant/env/hosts /etc/hosts
yes | cp -f /vagrant/env/ssh_keys/id_rsa* /root/.ssh/
cat /root/.ssh/id_rsa.pub > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/id_rsa*
yes | cp -f /vagrant/env/sshd_config /etc/ssh/
service sshd restart
swapoff -a

apt-get install nano curl -y
curl https://get.docker.com/ | bash
# done
echo "Done!"